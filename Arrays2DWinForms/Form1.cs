﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays2DWinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
        }

         double[] arr;


        private void button1_Click(object sender, EventArgs e)
        {
          
            int count = Convert.ToInt32(up.Text);
            dataGridViewArray.RowCount = 1;
            dataGridViewArray.ColumnCount = count;
            arr = new double[count];
           
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(-15000, 20000) / 100.0;
                dataGridViewArray[i, 0].Value = arr[i];
                dataGridViewArray.Columns[i].HeaderText = i.ToString();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            double sum = 0;
            Array.Sort(arr);
            for (int i = 0; i < arr.Length; i++)
                dataGridViewArray[i, 0].Value = arr[i];
            for (int i = 0; i < arr.Length; i++)
                if (arr[i] > 0)
                    sum += arr[i];
            TextBoxSumma.Text = sum.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
