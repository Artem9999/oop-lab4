﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays2DWinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
        }

        double[,] arr;


        private void button1_Click(object sender, EventArgs e)
        {
            // ======== Конвертирование данных ========
            int u = Convert.ToInt32(up.Text);
            int down = Convert.ToInt32(dwn.Text);
            dataGridArrays.RowCount = down;
            dataGridArrays.ColumnCount = u;
            arr = new double[u, down];
            // ======== ========
            for (int j = 0; j < arr.GetLength(0); j++)
                dataGridArrays.Rows[j].HeaderCell.Value = j.ToString();
            for (int i = 0; i < arr.GetLength(1); i++)
            {
                dataGridArrays.Columns[i].HeaderText = i.ToString();
            }
            // ======== Генерация псевдослучайных чисел ========
            Random rnd = new Random();
            double max = 110.35;
            double min = -110.34;
            for (int i = 0; i < down; i++)
            {
                for (int j = 0; j < u; j++)
                {
                    arr[i, j] = rnd.NextDouble() * (max - min) + min;
                    arr[i, j] = Math.Round(arr[i, j], 2);
                    dataGridArrays[j, i].Value = arr[i, j];
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int u = Convert.ToInt32(up.Text);
            int down = Convert.ToInt32(dwn.Text);
            int k = Convert.ToInt32(TextBoxK.Text);
            double temp = 0;
            for (int i = 0; i < u; i++)
            {
                for (int a = 1; a <= k; a++)
                {
                    if (i % 2 == 0)
                    {
                        temp = arr[i, down - 1];
                        for (int j = down - 1; j > 0; j--)
                        {
                            arr[i, j] = arr[i, j - 1];
                        }
                        arr[i, 0] = temp;
                    }
                }
            }
            for (int i = 0; i < u; i++)
            {
                for (int j = 0; j < down; j++)
                {
                    dataGridArrays[j, i].Value = arr[i, j];

                }
            }
            for (int i = 0; i < u; i++)
            {
                double sum = 0;
                for (int j = 0; j < down; j++)
                {
                    sum += arr[i, j];
                }
                sum = Math.Round(sum, 2);
                TextBoxSumma.Items.Add($"{i + 1} рядка: {sum}");
            }
        }


    }
}
    

