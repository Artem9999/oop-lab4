﻿using System;
using System.Linq;

namespace Arrays1DConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество элементов(n):");
            int N = Convert.ToInt32(Console.ReadLine());

            double[] array = new double[N];

            Random random = new Random();

            double max = 10.53;
            double min = -10.51;
        
            for (int i = 0; i < N; i++)
            {
                array[i] = random.NextDouble() * (max - min) + min;
            }
            
            int count = 0; double sum = 0;
            for (int i = 0; i < N; i++)
            {
                if ((array[i] % 3) == 0)
                    count++;
                sum = array[i];
            }
            Console.WriteLine($"Сумма элементов с индексами которые делятся на три {sum}");

            foreach (var v in array)
                Console.WriteLine("{0:0.00}", v);
            
            Console.WriteLine("Отсортированые элементы :");
            for (int i = 0; i < array.Length / 2; i++)
            {
                for (int j = 0; j < array.Length / 2 - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        double z = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = z;
                    }
                }
            }
            foreach (var v in array)
                Console.WriteLine("{0:0.00}", v);


        }
    }
}